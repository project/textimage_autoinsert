
This module allows the easy insertion of Text Image presets into content or page variables through a GUI without the use of PHP.

However, installing this module on Drupal 5 also requires a small modification to template.php in your phptemplate theme.
In template.php find the function _phptemplate_variables() and right before this line:

  return $vars;

Insert this line:

  ($m_vars = module_invoke_all('manipulate_phptemplate_vars', $hook, $vars)) ? $vars = $m_vars : 0;

After you create your Text Image presets you will be able to create Autoinsert presets which define where Text Images are to be inserted.
